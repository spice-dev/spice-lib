# SPICE libraries

This contains a set of libraries needed to run SPICE code. Some licenses apply, see respective packages for the licensing info, however, all of these libraries are open source. We will probably convert some of them to submodules, if they are maintained on some public git repository, once some free time magically appears.

## Compilation

It is easy, just follow these steps:

* Create a configuration file `config/environment.sh`. There are some samples present in `config/samples`. If you happen to use some computers that we at IPP did, you just have to change the installation directory. All you have to do is specify paths to system libraries. We were using MKL suite for BLAS etc.
  * If there is no MKL installed, you have to install BLAS, LAPACK, BLACS and ScaLAPACK (in this order) to run MUMPS.
  * These libraries are here, however, you have to install them yourself (see **No BLAS** section below).
* Run the `install_libs.sh` script.
* Check your installation directory for installed libraries (this will be someday done automatically).
* You are ready.

In case the last point does not apply, check the log files for missing dependencies and install them as well.

### No BLAS on your machine

Run `extract_libs.sh` only. Then go to all respective directories (BLAS, LAPACK, BLACS and ScaLAPACK) and run installation scripts. Update your configuration file to point to BLAS (etc.) libraries directly (mostly MUMPS -- all of mentioned --; and UF are affected -- only BLAS & LAPACK). Then proceed with installation by running `install_libs.sh`. You may need to update SPICE configuration files as well in spice2/spice3 repositories.

## Changelog

* 2019-10-09: AP refactored the installation scripts and added the support for arbitrary installation directory with common Linux structure.
* 2018-ish: The package was separated from spice2 repo for reusability both in spice2 and spice3 (and maybe spicyl).
* 2017-ish: The package was inside spice2 repo.
