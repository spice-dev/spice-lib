#!/bin/bash

# This will extract external library sources from archives
# If you are replacing a library, you should correct the compile script, as well as customization of makefiles (see directory customization)

source ./config/environment.sh

function extract_library_dir {
    LIB_NAME=$1
    LIB_DIR=$2
    LIB_EXTRACTOR=$3
    LIB_ARCHIVE=$4
    
    # Create installation directory
    mkdir -p $PICLIB_DIR/$LIB_DIR
    cd $PICLIB_DIR/$LIB_DIR
    
    # Extract library archive to installation directory
    EX_CMD="$LIB_EXTRACTOR $PICLIB_BASE/archives/$LIB_ARCHIVE"
    echo $EX_CMD
    eval $EX_CMD
    
    # Create installation script
    SCRIPT_PATH=$PICLIB_DIR/$LIB_NAME/compile_$LIB_NAME.sh
    echo "#!/bin/bash">$SCRIPT_PATH
    echo -e "# This is an automatically generated compilation script for $LIB_NAME.\n">>$SCRIPT_PATH
    echo -e "source $PICLIB_BASE/config/environment.sh\n">>$SCRIPT_PATH
    cat $PICLIB_BASE/scripts/compile_$LIB_NAME.sh>>$SCRIPT_PATH
    chmod +x $SCRIPT_PATH

    # overwrite configuration with SPICE lib environmental variables
    cp -rvf $PICLIB_BASE/customization/$LIB_NAME/* $PICLIB_DIR/$LIB_NAME/
}

# Simple wrapper function for default installation naming scheme
function extract_library {
    extract_library_dir $1 $1 "$2" $3
}

echo "SPICE libs extraction is about to begin"

# Uncomment to remove the source directory first

# echo "Removing old sources"
# rm -rf ./source
mkdir -p $PICLIB_DIR
mkdir -p $PICLIB_INSTALL
mkdir -p $PICLIB_INSTALL/bin
mkdir -p $PICLIB_INSTALL/include
mkdir -p $PICLIB_INSTALL/lib

# extract hypre poisson solver
extract_library hypes "tar -xzf" hypes.tar.gz

# extract f2c
extract_library_dir f2c "f2c/f2c" "unzip -u" libf2c.zip

# extract hypre
extract_library hypre "tar -xzf" hypre.tar.gz

# extract hdf5
extract_library hdf5 "tar -xzf" hdf5-1.10.5.tar.gz

# extract suite sparse
extract_library suitesparse "tar -xzf" SuiteSparse-5.8.1.tar.gz

# extract BLAS libraries
extract_library blas "tar -xzf" OpenBLAS-0.2.20.tar.gz
extract_library blas "tar -xzf" GotoBLAS2-1.13.tar.gz

# extract Lapack
extract_library lapack "tar -xzf" lapack-3.4.2.tgz

# extract BLACS (multiple archives)
extract_library blacs "tar -xzf" nxblacs.tgz 
extract_library blacs "tar -xzf" pvmblacs.tgz 
extract_library blacs "tar -xzf" mpiblacs.tgz 
extract_library blacs "tar -xzf" mpiblacs-patch03.tgz 

# extract scalapack library
extract_library scalapack "tar -xzf" scalapack.tgz

# extract clock utility
extract_library clock "tar -xzf" clock.tar.gz

# extract memusage utility
extract_library memusage "tar -xzf" memusage.tar.gz

# extract matio library
# Matio cannot be replaced with newer version since Fortran support was removed.
extract_library matio "tar -xzf" matio-1.3.4.tar.gz

# extract scotch library
extract_library scotch "tar -xzf" scotch_5.1.12b_esmumps.tar.gz
extract_library scotch "tar -xzf" scotch_6.0.6.tar.gz

# extract mumps library
extract_library mumps "tar -xzf" MUMPS_4.10.0.tar.gz
extract_library mumps "tar -xzf" MUMPS_5.1.2.tar.gz
extract_library mumps "tar -xzf" MUMPS_5.3.5.tar.gz

# extract umfpack library
extract_library UF "tar -xzf" AMD-2.2.0.tar.gz
extract_library UF "tar -xzf" UFconfig-3.1.0.tar.gz
extract_library UF "tar -xzf" UMFPACK.tar.gz

echo "SPICE libs are ready for compilation"
