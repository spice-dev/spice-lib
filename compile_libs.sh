#!/bin/bash

source ./config/environment.sh

# compile all external libraries which are needed for SPICE

function compile_library {
    LIB_NAME=$1
   
    cd $PICLIB_DIR/$LIB_NAME
    pwd
    ./compile_$LIB_NAME.sh > compile.log 2>compile.err

    echo "Library $LIB_NAME done"
}

echo "SPICE libs compilation is about to begin"

# Let's be sure about the directory... (this script should be in $PICLIB_BASE, but compile_{0}.sh can change it)
# This compiles only necessary libraries.
# BLAS is expected to be already present on the computer; however, it can be compiled from sources found here.

compile_library clock
compile_library memusage
compile_library f2c
compile_library matio
compile_library scotch
compile_library mumps
compile_library UF
compile_library hdf5
compile_library hypre
compile_library hypes
