# This is a include file for make.
#
# You can either use the file without any changes, or you should specify your own paths.
#
#   *   If no changes are made, you should compile all external libraries first.
#   *   If you are using your own libraries, be sure that all of them are compiled with the same compiler (eg. do not mix intelmpi and openmpi etc.). Change only appropriate assignments then.

# standard makefile shell assignment
SHELL = /bin/bash

FC=$(PICLIB_FC) -fpp
MPIF90=$(PICLIB_MPIF90) -fpp

DEBUG_FLAGS=-C -g  -check all -check noarg_temp_created
RELEASE_FLAGS=-DALLOW_NON_INIT -O3 -axCORE-AVX2 -xSSE4.2 -qopt-report1 -qopt-report-phase=vec -qoverride-limits -qopenmp -fPIC
#DEBUG_FLAGS=
#FC_FLAGS=-fPIC -r8 -axCORE-AVX2 -xSSE4.2
FC_FLAGS=-fpp -DPES3D_TESTING -fPIC

CLOCK_LIB=$(PICLIB_INSTALL)/lib/libclock.a
MEMUSAGE_LIB=$(PICLIB_INSTALL)/lib/libmemusage.a
F2C_LIB=$(PICLIB_INSTALL)/lib/libf2c.a

MATIO_INC=-I$(PICLIB_INSTALL)/include
MATIO_LIBS=$(PICLIB_INSTALL)/lib/libmatio.a -lz

FC_FLAGS_MKL=$(FC_FLAGS) -qopenmp -I$(MKLROOT)/include

MKL_LIBS=-Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_ilp64.a $(MKLROOT)/lib/intel64/libmkl_core.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_blacs_intelmpi_lp64.a -Wl,--end-group -lpthread -lm -ldl

HYPRE_INCDIR=$(PICLIB_INSTALL)/include
HYPRE_LIBDIR=$(PICLIB_INSTALL)/lib

ALL_LIBS=$(CLOCK_LIB) $(MEMUSAGE_LIB) $(MATIO_LIBS) -L$(HYPRE_LIBDIR) -lHYPRE -lstdc++ $(MKL_LIBS) -I$(HYPRE_INCDIR)
ALL_INC=-I$(PICLIB_INSTALL)/include/matio/matio-1.3.4/src -I$(HYPRE_INCDIR)

PREFIX=$(PICLIB_INSTALL)