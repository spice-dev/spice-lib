#!/bin/bash

module purge
module load intelstudio/18
module load intelmpi/18

export SPICE_BASE=/scratch/podolnik/spice
export PICLIB_DIR=$SPICE_BASE/spice-lib/source
export PICLIB_INSTALL=$SPICE_BASE/spice-lib/_install
export PICLIB_MPI_DIR=/sw/intel/compilers_and_libraries_2018.3.222/linux/mpi/intel64

# This probably does not have to be changed.

export PICLIB_FC=ifort
export PICLIB_MPIF90=mpiifort

export PICLIB_F77=ifort
export PICLIB_MPIF77=mpiifort

export PICLIB_CC=icc
export PICLIB_MPICC=mpiicc

export PICLIB_MKL_INC="-I$MKLROOT/include/intel64/lp64 -I$MKLROOT/include"
export PICLIB_MKL_CORE="-Wl,--start-group $MKLROOT/lib/intel64/libmkl_intel_lp64.a $MKLROOT/lib/intel64/libmkl_intel_thread.a $MKLROOT/lib/intel64/libmkl_core.a $MKLROOT/lib/intel64/libmkl_blacs_intelmpi_lp64.a -Wl,--end-group -liomp5 -lpthread -lm -ldl"
export PICLIB_BLAS="$MKLROOT/lib/intel64/libmkl_blas95_lp64.a"
export PICLIB_LAPACK="$MKLROOT/lib/intel64/libmkl_lapack95_lp64.a"
export PICLIB_SCALAPACK="$MKLROOT/lib/intel64/libmkl_scalapack_lp64.a "

export PICLIB_MPI_INC=$PICLIB_MPI_DIR/include

export PICLIB_INC="$PICLIB_MKL_INC"
export PICLIB_OTHERS="$PICLIB_MKL_CORE"

export PICLIB_MPIRUN=mpirun

export PICLIB_FC64="ifort -i8"
export PICLIB_CC64="icc -DMKL_ILP64"
export PICLIB_MKL_INC64="-I$MKLROOT/include/intel64/ilp64 -I$MKLROOT/include"
export PICLIB_MKL_CORE64="-Wl,--start-group $MKLROOT/lib/intel64/libmkl_intel_ilp64.a $MKLROOT/lib/intel64/libmkl_intel_thread.a $MKLROOT/lib/intel64/libmkl_core.a $MKLROOT/lib/intel64/libmkl_blacs_intelmpi_ilp64.a -Wl,--end-group -liomp5 -lpthread -lm -ldl"
export PICLIB_BLAS64="$MKLROOT/lib/intel64/libmkl_blas95_ilp64.a"
export PICLIB_LAPACK64="$MKLROOT/lib/intel64/libmkl_lapack95_ilp64.a"
export PICLIB_SCALAPACK64="$MKLROOT/lib/intel64/libmkl_scalapack_ilp64.a "
export PICLIB_INC64="$PICLIB_MKL_INC64"
export PICLIB_OTHERS64="$PICLIB_MKL_CORE64"

export PICLIB_CFLAGS="$PICLIB_MKL_INC"
export PICLIB_FFLAGS="$PICLIB_MKL_INC"

echo "Environment variables exported"