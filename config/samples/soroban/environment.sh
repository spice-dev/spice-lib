#!/bin/bash

module purge
module load intelstudio/15
module load openmpi/1.10.2-intel15

export PICLIB_DIR=/scratch/podolnik/spice-lib/_source

export PICLIB_FC=ifort
export PICLIB_MPIF90=mpif90

export PICLIB_F77=ifort
export PICLIB_MPIF77=mpif90

export PICLIB_CC=icc
export PICLIB_MPICC=mpicc

export PICLIB_MPI_DIR=/sw/openmpi/1.10.2/intel-15
export PICLIB_MPI_INC=/sw/openmpi/1.10.2/intel-15/include

export PICLIB_MPIRUN=mpirun

