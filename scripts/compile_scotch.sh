# compile scotch

# old version
# cd scotch_5.1.12_esmumps/src
# make clean
# 
# make scotch
# make ptscotch
# 
# cd ../..

cd scotch_6.0.6/src
make clean

make scotch
make ptscotch
make esmumps
make ptesmumps

# sometimes it fails due to bad timing... better to do this twice
make scotch
make ptscotch
make esmumps
make ptesmumps

make install
