# compile the hdf5 library

cd hdf5-1.10.5
make clean

CC=$PICLIB_MPICC
FC=$PICLIB_MPIF90

./configure --prefix=$PICLIB_INSTALL --enable-fortran

make
make check
make install
make check-install

