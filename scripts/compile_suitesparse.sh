export F77=$PICLIB_FC -I../Include

# compile the umfpack library

cd SuiteSparse
make clean

# make the library and fortran interface
make

cd UMFPACK
make fortran
make fortran64
