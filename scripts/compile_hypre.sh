# compile hypre

cd hypre/src

./configure --prefix=$PICLIB_INSTALL
make
make install

cp -f $PICLIB_BASE/customization/hypre/hypre/src/include/HYPREf.h $PICLIB_INSTALL/include/HYPREf.h
cp -f $PICLIB_BASE/customization/hypre/hypre/src/include/HYPREf.h $PICLIB_DIR/hypre/hypre/src/hypre/include/HYPREf.h
