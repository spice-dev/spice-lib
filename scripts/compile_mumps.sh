#!/bin/bash

# this line is here for proper referencing from working directory
# if batch compiling is active, error is raised, but does not influence
# the compilation 
source ../../config/environment.sh

# compile mumps parallel solver

# old version
# cd MUMPS_4.10.0
# make clean
# 
# make

# new version
# cd MUMPS_5.1.2
# 
# make clean
# make
# 
# cp -vf lib/*.a $PICLIB_INSTALL/lib/
# cp -vf include/*.h $PICLIB_INSTALL/include/ 

# new version
cd MUMPS_5.3.5

make clean
make

cp -vf lib/*.a $PICLIB_INSTALL/lib/
cp -vf include/*.h $PICLIB_INSTALL/include/ 
