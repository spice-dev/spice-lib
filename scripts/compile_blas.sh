# compile OpenBLAS (edit appropriate variables in environment.sh)

cd OpenBLAS-0.2.20
make clean

make FC=$PICLIB_FC CC=$PICLIB_CC BINARY=64 NO_PARALLEL_MAKE=1 INTERFACE64=1

cd ..

cd GotoBLAS2

make clean
make TARGET=NEHALEM BINARY=64 FC=$PICLIB_FC CC=$PICLIB_CC 
