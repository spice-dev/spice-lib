cd matio-1.3.4
make clean

export CC=$PICLIB_CC
export FC=$PICLIB_FC
export F77=$PICLIB_F77

# configure matio library with the fortran interface

./configure --enable-fortran --prefix=$PICLIB_INSTALL
make
make install

