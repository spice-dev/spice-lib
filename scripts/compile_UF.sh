# compile the umfpack library

cd UMFPACK
make clean

# make the library and fortran interface
make all
make fortran
make fortran64

# copy all stuff to some better place
cp -v Lib/*.a $PICLIB_INSTALL/lib/
cp -v Include/*.h $PICLIB_INSTALL/include/
cp -v Demo/*wrapper*.o $PICLIB_INSTALL/lib/

cp -v ../AMD/Lib/*.a $PICLIB_INSTALL/lib/
cp -v ../AMD/Include/*.h $PICLIB_INSTALL/include/

