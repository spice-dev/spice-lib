#!/bin/bash

# If you believe that everything is set up correctly, you can run this script and wait.
# Check logs for errors. Logs are created for the extraction and compilation/copy parts. The checking part prints its output to the console.

source ./config/environment.sh

cd $PICLIB_BASE
./extract_libs.sh > extract.log 2>&1

cd $PICLIB_BASE
./compile_libs.sh > compile.log 2>&1
